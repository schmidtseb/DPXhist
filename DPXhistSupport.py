import numpy as np
import scipy.optimize
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '../energy_calibration/rebin/')
import rebin as rb

# === Rebinning ===
def rebinEnergyData(edges, data):
    isLarge = np.asarray([pixel for pixel in np.arange(len(data)) if pixel % 16 not in [0, 1, 14, 15]])
    return rebinEnergyHistExec(np.asarray(edges)[isLarge], np.asarray(data)[isLarge])

def rebinEnergyHist(edges, events, NPixel=192, plot=False):
    binsList, histList = [], []
    dataList = []

    events = np.asarray( events )

    # Total number of events
    NEvents = len(events)

    if plot:
        for i in range(len(edges)):
            plt.hist(events[events > edges[i][0]], bins=edges[i], alpha=0.5, density=False)

    for i in range(NPixel):
        # Get bin edges
        binEdges = edges[i]

        # Select data
        data = events

        # Discard empty events
        data = np.asarray( data )
        data = data[data > 0]
        dataList += list( data )

        # Calculate histogram
        try:
            hist, bins = np.histogram(data, bins=binEdges, density=False)
            hist += np.asarray(np.random.normal(0, np.sqrt(hist)), dtype=int)
            histList.append( hist ), binsList.append( bins )

            binsNew, histNew = rebinEnergyHistExec(binsList, histList)
        except:
            continue
            
        # Plot
        if plot:
            plt.step(binsNew[:-1], histNew, where='post') 
            hist, bins = np.histogram(dataList, bins=100)
            plt.step(bins[:-1], hist, where='post')
            plt.show()
            
    return binsNew, histNew

def rebinEnergyHistExec(binsList, histList):
    binsNew = np.sort(np.hstack(binsList))
    histNewList = []
    NPixel = len(binsList)
    for i in range(NPixel):
        bins, hist = binsList[i], histList[i]

        h = np.zeros(len(binsNew) - 1)
        for j in range(len(bins) - 2):
            if not hist[j]:
                continue

            b1, b2 = bins[j:j+2]
            # print b1, b2
            bw = float(b2 - b1)

            bCond = np.logical_and(binsNew[:-1] >= b1, binsNew[:-1] <= b2)
            h[bCond] = list(np.diff(binsNew[:-1][bCond]) / bw * hist[j]) + [0]

        # Last bin
        b1 = bins[-2]
        h[binsNew[:-1] >= b1] = hist[-1] / (bins[-1] + b1)
        histNewList.append( h )

    histNew = np.nan_to_num(np.nansum(histNewList, axis=0))

    # Remove duplicates
    histComb = sorted(list(set(zip(binsNew[:-1], histNew))))
    binsNew_, histNew = zip(*histComb)
    binsNew = list(binsNew_) + [binsNew[-1]]
    if histNew[0] == 0:
        histNew = histNew[1:]
        binsNew = binsNew[1:]
    histNew = np.asarray(histNew) / NPixel

    return binsNew, histNew

# === Deconvolution ===
def wiener_deconvolution(signal, kernel, lambd):
    "lambd is the SNR"
    kernel = np.hstack((kernel, np.zeros(len(signal) - len(kernel)))) # zero pad the kernel to same length
    H = np.fft.fft(kernel)
    deconvolved = np.real(np.fft.ifft(np.fft.fft(signal)*np.conj(H)/(H*np.conj(H) + lambd**2)))
    return deconvolved

def water_level_decon(y_meas, window, eps=0.1):
    padded = np.zeros_like(y_meas)
    padded[:window.size] = window

    yfreq = np.fft.fft(y_meas)
    winfreq = np.fft.fft(padded)

    winfreq[winfreq < eps] = eps
    newfreq = yfreq / winfreq 

    return np.fft.ifft(newfreq)

# === Bin edges generation ===
def getBinEdgesRandom(NPixels, edgeMin, edgeMax, edgeOvfw, uniform=False, paramDict=None):
    edgeList = []
    for pixel in range(NPixels):
        if uniform:
            # Calculate bin edges
            binEdges = np.sort(np.random.uniform(edgeMin, edgeMax, 15))
            binEdges = np.insert(binEdges, 0, edgeMin)
            binEdges = np.append(binEdges, edgeOvfw)
        else:
            pixelOffset = 2
            if paramDict is not None:
                while pixel + pixelOffset not in paramDict.keys():
                    pixelOffset += 4
                params = paramDict[pixel + pixelOffset]

                a, b, c, t = params['a'], params['b'], params['c'], params['t']
                if 'h' in params.keys():
                    h, k = params['h'], params['k']
                else:
                    h, k = 1, 0

                # print( a, b, c, t, h, k )
                # Get min, max and overflow edges
                edgeMin_ = EnergyToToTSimple(edgeMin, a, b, c, t, h, k)
                edgeMax_ = EnergyToToTSimple(edgeMax, a, b, c, t, h, k)
                edgeOvfw_ = EnergyToToTSimple(edgeOvfw, a, b, c, t, h, k)
                if edgeMin_ > edgeMax_:
                    edgeMin_, edgeMax_ = edgeMax_, edgeMin_
                # print (edgeMin_, edgeMax_, edgeOvfw_)

                # Get bin edges with noisy evenly spaced distances
                binEdges = np.around(getBinEdgesRandomEvenSpace(edgeMin_, edgeMax_, edgeOvfw_))

                # Convert back to energy
                binEdges = ToTtoEnergySimple(np.asarray(binEdges), a, b, c, t, h, k)
                if any(np.isnan(binEdges)):
                    binEdges = getBinEdgesRandomEvenSpace(edgeMin, edgeMax, edgeOvfw)
                # print( binEdges )
                # print

            else:
                binEdges = getBinEdgesRandomEvenSpace(edgeMin, edgeMax, edgeOvfw)

        edgeList.append( binEdges )
    return edgeList

def getBinEdgesRandomEvenSpace(edgeMin, edgeMax, edgeOvfw):
    # Mean difference 
    diff = float(edgeMax - edgeMin) / 15
    binDiff = np.random.normal(diff, diff / 3., 15)
    binDiff -= (np.sum(binDiff) - (edgeMax - edgeMin)) / 15
    binEdges = np.cumsum(binDiff) + edgeMin
    binEdges = np.insert(binEdges, 0, 1.5 * edgeMin + abs(np.random.normal(0, 0.5*diff)))
    binEdges = np.append(binEdges, edgeOvfw)
    binEdges = np.sort( binEdges )

    return binEdges

def getBinEdgesUniform(NPixels, edgeMin, edgeMax, edgeOvfw):
    edgeList = []
    xInit = np.linspace(edgeMin, edgeMax, 16)
    bw = xInit[1] - xInit[0]
    pixelIdx = 0
    for pixel in range(NPixels):
        if not isLarge(pixel):
            edgeList.append(np.append(xInit, edgeOvfw))
            continue

        offset = bw / 192. * pixelIdx
        edgeList.append(np.append(xInit + offset, edgeOvfw))
        pixelIdx += 1
    return edgeList

# === Window estimation ===
def getRebinedHist(binEdges, data, plot=False):
    bins, hist = rebinEnergyHist(binEdges, data, NPixel=192, plot=False)
    bins, hist = np.asarray(bins), np.asarray(hist)
    binsLast = bins[-1]
    bins = bins[:-1][hist > 0]
    bins = np.append(bins, binsLast)
    hist = hist[hist > 0]
    
    if plot:
        fig, ax = plt.subplots(figsize=(10, 3))
        ax.step(bins[:-1], hist, where='post')
        ax.set_xlabel('Energy (keV)')
        ax.set_ylabel('Counts')
        plt.show()

    return bins, hist

def estimateWindow(x, binEdges, windowFunc, meanRange=np.arange(100, 150, 5), NEntries=10000, plot=False):
    windowTauList = []
    for idx, mean in enumerate(meanRange):
        data = [mean] * NEntries
        bins, hist = getRebinedHist(binEdges, data)
        y = np.nan_to_num(rb.rebin(bins, hist, x, interp_kind='piecewise_constant'))
        # y = scipy.signal.savgol_filter(y, 31, 3)

        p0 = (max(y), mean, 5)
        popt, pcov = scipy.optimize.curve_fit(windowFunc, x[:-1], y, p0=p0)
        print( p0, popt )
        windowTau = popt[-1]
        windowTauList.append( windowTau )
        
        if plot:
            color = 'C%d' % idx
            plt.plot(bins[:-1], np.asarray(hist, dtype=float)/np.max(hist), color=color)
            plt.plot(x[:-1], y/np.max(y), ls='--', color=color)

            yFit = windowFunc(bins[:-1], *popt)
            plt.plot(bins[:-1], yFit / np.max(yFit), ls='-.', color=color)
            plt.xlabel('Energy (keV)')
            plt.ylabel('Normalized counts')
            plt.xlim(20, 80)

    windowTau = np.mean(windowTauList)
    print(windowTau)
    return windowTau

# === Window functions ===
def expWindow(x, A, mu, tau):
    return A * np.exp(-np.abs(x - mu) / float(tau))

def triangleWindow(x, A, mu, tau):
    return np.where(np.logical_or(x < (mu - tau), x > (mu + tau)), 0, A * (1 - np.abs(x - mu) / float(tau)))

# === Temperature Correction ===
def correctTemperature(binEdgesDict, tempDict, tempCalibDict, slot=1):
    T = np.mean(tempDict['temp'])
    Toffset = tempCalibDict['Toffset']
    slope, offset = tempCalibDict['slope'], tempCalibDict['offset']

    tempCalibDict.keys()
    binEdgesCorrDict = {'Slot%d' % slot: []}
    for pixel in range(256):
        b = binEdgesRandomDict['Slot%d' % slot][pixel]
        if pixel not in paramsDict.keys():
            binEdgesDict['Slot%d' % slot].append( b )
            continue

        p = paramsDict[pixel]
        bToT = tte.EnergyToToT(b, p['a'], p['b'], p['c'], p['t'], p['h'], p['k'])
        b_new = pttt.getDataAtTSingle(bToT, T, slope[pixel], offset[pixel], Toffset, paramsDict[pixel], energy=True)
        if np.any(np.isnan(b_new)):
            binEdgesDict['Slot%d' % slot].append( b )
        else:
            binEdgesDict['Slot%d' % slot].append( b_new )
            
    return binEdgesCorrDict

